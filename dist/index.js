"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express")); //eslint-disable-line
const body_parser_1 = require("body-parser");
const mongoose_1 = __importDefault(require("mongoose"));
const multer_1 = __importDefault(require("multer"));
const uuid_1 = require("uuid"); //eslint-disable-line
const path_1 = __importDefault(require("path"));
const recipes_1 = __importDefault(require("./routes/recipes"));
const app = express_1.default();
app.use(body_parser_1.json());
app.use('/images', express_1.default.static(path_1.default.join(__dirname, 'images')));
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    if (req.method === 'OPTIONS') {
        return res.sendStatus(200);
    }
    next();
});
const storage = multer_1.default.diskStorage({
    destination(req, file, cb) {
        cb(null, 'dist/images');
    },
    filename(req, file, cb) {
        cb(null, uuid_1.v4());
    },
});
const fileFilter = (req, file, cb) => {
    if (file.mimetype === 'image/png' || file.mimetype === 'image/jpg' || file.mimetype === 'image/jpeg') {
        cb(null, true);
    }
    else {
        cb(null, false);
    }
};
app.use(multer_1.default({ storage, fileFilter }).single('image'));
app.use('/recipes', recipes_1.default);
app.use((err, req, res, next) => {
    const status = err.statusCode || 500;
    res.status(status).json({ message: err.message });
});
mongoose_1.default.connect('mongodb+srv://mohannedm:zip123@cluster0.usvsi.mongodb.net/recipes?retryWrites=true&w=majority')
    .then(() => {
    app.listen(4000);
});

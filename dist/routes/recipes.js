"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const express_validator_1 = require("express-validator");
const RecipesController = __importStar(require("../controllers/recipes"));
const router = express_1.Router();
router.post('/', [
    express_validator_1.check('title').trim().isString().isLength({ min: 5, max: 50 }),
    express_validator_1.check('ingredients').isString(),
    express_validator_1.check('recipe').trim().isString().isLength({ min: 5, max: 500 }),
], RecipesController.createRecipe);
router.get('/', RecipesController.getRecipes);
router.get('/:id', RecipesController.getRecipe);
router.put('/:id', [
    express_validator_1.check('title').trim().isString().isLength({ min: 5, max: 50 }),
    express_validator_1.check('ingredients').isString(),
    express_validator_1.check('recipe').trim().isString().isLength({ min: 5, max: 500 }),
], RecipesController.editRecipe);
router.delete('/:id', RecipesController.deleteRecipe);
exports.default = router;

"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteRecipe = exports.editRecipe = exports.getRecipe = exports.getRecipes = exports.createRecipe = void 0;
const express_validator_1 = require("express-validator");
const Recipe_1 = __importDefault(require("../models/Recipe"));
const createRecipe = async (req, res, next) => {
    try {
        const errors = express_validator_1.validationResult(req);
        if (!errors.isEmpty()) {
            const error = new Error('Validation error! Please check your inputs.');
            error.statusCode = 400;
            throw error;
        }
        const { title } = req.body;
        const { ingredients } = req.body;
        const { recipe } = req.body;
        let imageUrl;
        if (req.file) {
            imageUrl = req.file.path.replace('\\', '/');
            imageUrl = imageUrl.replace('dist', '');
            if (!imageUrl)
                throw new Error('image upload failed');
        }
        else {
            imageUrl = '';
        }
        const decodedIngredients = JSON.parse(ingredients);
        const newRecipe = new Recipe_1.default({
            title,
            ingredients: decodedIngredients,
            recipe,
            image: imageUrl,
        });
        await newRecipe.save();
        res.status(201).json({ recipe: newRecipe });
    }
    catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
};
exports.createRecipe = createRecipe;
const getRecipes = async (req, res, next) => {
    try {
        const recipes = await Recipe_1.default.find({});
        res.status(200).json({ recipes });
    }
    catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
};
exports.getRecipes = getRecipes;
const getRecipe = async (req, res, next) => {
    try {
        const recipe = await Recipe_1.default.findById(req.params.id);
        if (!recipe)
            throw new Error('Recipe not found');
        res.status(200).json({ recipe });
    }
    catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
};
exports.getRecipe = getRecipe;
const editRecipe = async (req, res, next) => {
    try {
        const _id = req.params.id;
        const returnedRecipe = await Recipe_1.default.findById(_id);
        if (!returnedRecipe)
            throw new Error('Recipe not found');
        const errors = express_validator_1.validationResult(req);
        if (!errors.isEmpty()) {
            const error = new Error('Validation error! Please check your inputs.');
            error.statusCode = 400;
            throw error;
        }
        const { title } = req.body;
        const { ingredients } = req.body;
        const { recipe } = req.body;
        let imageUrl;
        const editedData = {};
        if (title)
            editedData.title = title;
        if (ingredients)
            editedData.ingredients = JSON.parse(ingredients);
        if (recipe)
            editedData.recipe = recipe;
        if (req.file) {
            imageUrl = req.file.path.replace('\\', '/');
            imageUrl = imageUrl.replace('dist', '');
            if (!imageUrl)
                throw new Error('image upload failed');
            editedData.image = imageUrl;
        }
        else {
            imageUrl = '';
        }
        await Recipe_1.default.findOneAndUpdate({ _id }, editedData);
        const editedRecipe = await Recipe_1.default.findById(_id);
        res.status(201).json({ recipe: editedRecipe });
    }
    catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
};
exports.editRecipe = editRecipe;
const deleteRecipe = async (req, res, next) => {
    try {
        const _id = req.params.id;
        const recipe = await Recipe_1.default.findById(_id);
        if (!recipe)
            throw new Error('Recipe not found');
        await Recipe_1.default.deleteOne({ _id });
        res.status(201).json({ recipe });
    }
    catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
};
exports.deleteRecipe = deleteRecipe;

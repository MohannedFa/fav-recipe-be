import { Router } from 'express'
import { check } from 'express-validator'
import * as RecipesController from '../controllers/recipes'

const router = Router()

router.post('/', [
  check('title').trim().isString().isLength({ min: 5, max: 50 }),
  check('ingredients').isString(),
  check('recipe').trim().isString().isLength({ min: 5, max: 500 }),
], RecipesController.createRecipe)

router.get('/', RecipesController.getRecipes)

router.get('/:id', RecipesController.getRecipe)

router.put('/:id', [
  check('title').trim().isString().isLength({ min: 5, max: 50 }),
  check('ingredients').isString(),
  check('recipe').trim().isString().isLength({ min: 5, max: 500 }),
], RecipesController.editRecipe)

router.delete('/:id', RecipesController.deleteRecipe)

export default router

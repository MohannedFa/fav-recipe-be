import { RequestHandler } from 'express' //eslint-disable-line
import { validationResult } from 'express-validator'
import { CreateRecipeRequest, GetRecipeRequest, GetRecipesRequest, JsonError, EditRecipeRequest } from '../types/recipes' //eslint-disable-line
import Recipe from '../models/Recipe'

export const createRecipe: RequestHandler = async (req: CreateRecipeRequest, res, next) => {
  try {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      const error: JsonError = new Error('Validation error! Please check your inputs.')
      error.statusCode = 400
      throw error
    }

    const { title } = req.body
    const { ingredients } = req.body
    const { recipe } = req.body
    let imageUrl
    if (req.file) {
      imageUrl = req.file.path.replace('\\', '/')
      imageUrl = imageUrl.replace('dist', '')
      if (!imageUrl) throw new Error('image upload failed')
    } else {
      imageUrl = ''
    }
    const decodedIngredients = JSON.parse(ingredients)
    const newRecipe = new Recipe({
      title,
      ingredients: decodedIngredients,
      recipe,
      image: imageUrl,
    })

    await newRecipe.save()
    res.status(201).json({ recipe: newRecipe })
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  }
}

export const getRecipes: RequestHandler = async (req: GetRecipesRequest, res, next) => {
  try {
    const recipes = await Recipe.find({})
    res.status(200).json({ recipes })
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  }
}

export const getRecipe: RequestHandler = async (req: GetRecipeRequest, res, next) => {
  try {
    const recipe = await Recipe.findById(req.params.id)
    if (!recipe) throw new Error('Recipe not found')
    res.status(200).json({ recipe })
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  }
}

export const editRecipe: RequestHandler = async (req: EditRecipeRequest, res, next) => {
  try {
    const _id = req.params.id
    const returnedRecipe = await Recipe.findById(_id)
    if (!returnedRecipe) throw new Error('Recipe not found')

    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      const error: JsonError = new Error('Validation error! Please check your inputs.')
      error.statusCode = 400
      throw error
    }

    const { title } = req.body
    const { ingredients } = req.body
    const { recipe } = req.body
    let imageUrl
    const editedData: {[k: string]: any} = {}
    if (title) editedData.title = title
    if (ingredients) editedData.ingredients = JSON.parse(ingredients)
    if (recipe) editedData.recipe = recipe
    if (req.file) {
      imageUrl = req.file.path.replace('\\', '/')
      imageUrl = imageUrl.replace('dist', '')
      if (!imageUrl) throw new Error('image upload failed')
      editedData.image = imageUrl
    } else {
      imageUrl = ''
    }

    await Recipe.findOneAndUpdate({ _id }, editedData)

    const editedRecipe = await Recipe.findById(_id)

    res.status(201).json({ recipe: editedRecipe })
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  }
}

export const deleteRecipe: RequestHandler = async (req: GetRecipeRequest, res, next) => {
  try {
    const _id = req.params.id
    const recipe = await Recipe.findById(_id)
    if (!recipe) throw new Error('Recipe not found')

    await Recipe.deleteOne({ _id })
    res.status(201).json({ recipe })
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  }
}

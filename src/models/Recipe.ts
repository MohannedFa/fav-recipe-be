import mongoose, { Schema } from 'mongoose'
import { RecipeDocument } from '../types/recipes' //eslint-disable-line

const RecipeSchema = new Schema({
  title: {
    required: true,
    type: String,
  },
  ingredients: [{
    type: String,
    required: true,
  }],
  recipe: {
    required: true,
    type: String,
  },
  image: {
    type: String,
  },
})

export default mongoose.model<RecipeDocument>('Recipe', RecipeSchema)

import { Document } from 'mongoose' //eslint-disable-line
import { Request } from 'express' //eslint-disable-line

export interface RecipeDocument extends Document {
    title: string
    ingredients: string[]
    recipe: string
    image?: string
}

export interface CreateRecipeRequest extends Request{
    body: {
        title: string
        ingredients: string
        recipe: string
        image?: string
    }
}

export interface GetRecipesRequest extends Request{}

export interface GetRecipeRequest extends Request{}

export interface EditRecipeRequest extends Request{
    body: {
        title?: string
        ingredients?: string
        recipe?: string
        image?: string
    }
}

export interface DeleteRecipeRequest extends Request{
    params: {
        id: string
    }
}

export interface JsonError extends Error{
    statusCode?: number;
}

import express, { Response, Request, NextFunction } from 'express' //eslint-disable-line
import { json } from 'body-parser'
import mongoose from 'mongoose'
import multer from 'multer'
import { v4 } from 'uuid' //eslint-disable-line
import path from 'path'
import RecipesRoutes from './routes/recipes'
import { JsonError } from './types/recipes' //eslint-disable-line

const app = express()

app.use(json())

app.use('/images', express.static(path.join(__dirname, 'images')))

app.use((req, res, next) => { //eslint-disable-line
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE, OPTIONS')
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization')
  if (req.method === 'OPTIONS') {
    return res.sendStatus(200)
  }
  next()
})

const storage = multer.diskStorage({
  destination (req, file, cb) {
    cb(null, 'dist/images')
  },
  filename (req, file, cb) {
    cb(null, v4())
  },
})

const fileFilter = (req: any, file: any, cb: any) => {
  if (file.mimetype === 'image/png' || file.mimetype === 'image/jpg' || file.mimetype === 'image/jpeg') {
    cb(null, true)
  } else {
    cb(null, false)
  }
}
app.use(multer({ storage, fileFilter }).single('image'))
app.use('/recipes', RecipesRoutes)

app.use((err: JsonError, req: Request, res: Response, next: NextFunction) => { //eslint-disable-line
  const status = err.statusCode || 500
  res.status(status).json({ message: err.message })
})

mongoose.connect('mongodb+srv://mohannedm:zip123@cluster0.usvsi.mongodb.net/recipes?retryWrites=true&w=majority')
  .then(() => {
    app.listen(4000)
  })
